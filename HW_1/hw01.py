# ==================
# ДЗ №1: простые типы данных, изменяемые и неизменяемые типы, работа со строками, списки

# Задание: сделайте анализ выгрузки квартир с ЦИАН:

# 1) Измените структуру данных, используемую для хранения данных о квартире. Сейчас квартира = список. Сделайте
# вместо этого квартира = словарь следующего вида: flat_info = {"id":flat[0], "rooms":flat[1], "type":flat[2],
# "price":flat[11]}. В задании используйте поля: идентификатор квартиры на ЦИАН, количество комнат, тип (новостройка
# или вторичка), стоимость

# 2) Подсчитайте количество новостроек, расположенных у каждого из метро

import csv

# читаем информацию о квартирах в список flats_list
flats_list = list()
with open('output.csv', encoding="utf-8") as csvfile:
    flats_csv = csv.reader(csvfile, delimiter=';')
    flats_list = list(flats_csv)

# убираем заголовок
header = flats_list.pop(0)
# создаем словарь с информацией о квартирах
subway_dict = {}
for flat in flats_list:
    subway = flat[3].replace("м.", "")
    if subway != "":
      subway_dict.setdefault(subway, [])
      # TODO 1: добавьте код, который генерирует новую структуру данных с информацией о квартире - словарь вместо списка
      # ваш код...
      # не забудьте сделать проверку типа и преобразовать то, что можно, в числа

      flat_info = {}
      if flat[0].isdigit():
          flat_info.setdefault("id", int(flat[0]))
      else:
          flat_info.setdefault("id", flat[0])

      if flat[1].isdigit():
          flat_info.setdefault("rooms", int(flat[1]))
      else:
          flat_info.setdefault("rooms", flat[1])

      if flat[2].isdigit():
          flat_info.setdefault("type", int(flat[2]))
      else:
          flat_info.setdefault("type", flat[2])

      if flat[11].isdigit():
          flat_info.setdefault("price", int(flat[11]))
      else:
          flat_info.setdefault("price", flat[11])
      subway_dict[subway].append(flat_info)

# TODO 2: подсчитайте и выведите на печать количество новостроек, расположенных рядом с каждым из метро. Используйте вариант прохода по словарю, который вам больше нравится
# for k,v in subway_dict:
# используйте v для анализа значения
# ваш код...
# либо
new_building_count_all = 0
for k in subway_dict:
    new_building_count = 0
    for flat in subway_dict[k]:
        if flat['type'] == 'новостройка':
            new_building_count = new_building_count + 1
    print(f"У метро {k} {new_building_count} новостроек")
    new_building_count_all += new_building_count

print(f"Всего {new_building_count_all} новостроек")
